local core = require('openmw.core')
local world = require('openmw.world')
local types = require('openmw.types')
local storage = require('openmw.storage')
local I = require('openmw.interfaces')

local common = require('scripts.RandomNPC.common')
local npcsForCell = require('scripts.RandomNPC.cellData').npcsForCell

I.Settings.registerGroup {
    key = 'urm_RandomNPC_Gameplay',
    page = 'urm_RandomNPC',
    l10n = 'urm_RandomNPC',
    name = 'gameplay_name',
    permanentStorage = false,
    settings = {
        {
            key = 'limitPerCell',
            name = 'limitPerCell_name',
            default = 5,
            renderer = 'number',
            argument = {
                integer = true,
                min = 0,
            },
        },
        {
            key = 'npcLifetime',
            name = 'npcLifetime_name',
            description = 'npcLifetime_description',
            default = 6,
            renderer = 'number',
            argument = {
                integer = true,
                min = 0,
            },
        },
    },
}

local settings = storage.globalSection('urm_RandomNPC_Gameplay')

local function activeCellSet()
    local set = {}
    for _, a in pairs(world.activeActors) do
        if a.type == types.Player then
            set[common.cellId(a.cell)] = a
        end
    end
    return set
end

local randomNpcCounts = {}

return {
    engineHandlers = {
        onUpdate = function()
            if core.isWorldPaused() then return end
            local activeCells = activeCellSet()
            for cellId, player in pairs(activeCells) do
                if npcsForCell(cellId) then
                    local cellCount = randomNpcCounts[cellId] or 0
                    if cellCount < settings:get('limitPerCell') then
                        player:sendEvent(common.Events.RequestPosition, { cell = cellId })
                    end
                end
            end
        end,
        onSave = function()
            local saved = {}
            for cellId, count in pairs(randomNpcCounts) do
                if count ~= 0 then
                    saved[cellId] = count
                end
            end
            return saved
        end,
        onLoad = function(saved)
            if not saved then return end
            for cellId, count in pairs(saved) do
                randomNpcCounts[cellId] = count
            end
        end,
    },
    eventHandlers = {
        [common.Events.Spawn] = function(e)
            local existingCount = randomNpcCounts[e.cell] or 0
            if existingCount >= settings:get('limitPerCell') then
                return
            end
            randomNpcCounts[e.cell] = existingCount + 1
            local npcGroups = npcsForCell(e.cell)
            local position = e.position
            local group = npcGroups[math.random(#npcGroups)]
            local id = group[math.random(#group)]
            local obj = world.createObject(id, 1)
            obj:teleport(common.cellNameFromid(e.cell), position)
            obj:addScript('scripts/RandomNPC/npc.lua', { cell = e.cell })
        end,
        [common.Events.Despawn] = function(e)
            if e.npc.count > 0 then
                randomNpcCounts[e.cell] = math.max(0, (randomNpcCounts[e.cell] or 0) - 1)
                e.npc:remove()
            end
        end,
    }
}
