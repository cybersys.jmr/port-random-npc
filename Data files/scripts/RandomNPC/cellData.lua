local N = {
    cit = { "NM_npc_cit_1", "NM_npc_cit_2" },
    pil = { "NM_npc_pil_1", "NM_npc_pil_2" },
    red = { "NM_npc_redoran" },
    slv = { "NM_npc_slv_1", "NM_npc_slv_2", "NM_npc_slv_3", "NM_npc_slv_4" },
    citA = { "NM_npc_cit_A1", "NM_npc_cit_A2" },
    tel = { "NM_npc_tel_1", "NM_npc_tel_2" },
    miner = { "NM_npc_miner" },
    fish = { "NM_npc_fisherman" },
    TMora = { "NM_npc_tel_2", "NM_npc_cit_1" },
    adv = { "NM_npc_adv" },
}

local Locations = {
    ["-3 -3"] = { N.cit, N.cit, N.pil }, -- Балмора
    ["-3 -2"] = { N.cit, N.cit, N.pil }, -- Балмора
    ["-2 -2"] = { N.cit }, -- Балмора
    ["-4 -2"] = { N.cit, N.cit, N.slv }, -- Балмора

    ["-2 -9"] = { N.cit, N.fish }, -- Сейда Нин

    ["-3 6"] = { N.citA, N.citA, N.red }, -- Альдрун
    ["-2 6"] = { N.citA, N.pal, N.red }, -- Альдрун

    ["3 -9"] = { N.cit, N.pil }, -- территория рядом с Вивеком
    ["2 -10"] = { N.cit }, -- территория рядом с Вивеком
    ["5 -10"] = { N.cit }, -- территория рядом с Вивеком
    ["4 -11"] = { N.cit, N.cit, N.slv, N.adv, N.adv }, -- Вивек, Арена
    ["3 -10"] = { N.cit, N.cit, N.adv, N.pil }, -- Вивек, Квартал Чужеземцев
    ["4 -10"] = { N.cit }, -- Вивек, Квартал Чужеземцев
    ["2 -11"] = { N.cit, N.cit, N.slv }, -- Вивек, Хлаалу
    ["3 -11"] = { N.cit, N.cit, N.red }, -- Вивек, Редоран
    ["3 -12"] = { N.cit }, -- Вивек, Делин
    ["4 -12"] = { N.cit }, -- Вивек, Олмс
    ["5 -11"] = { N.cit, N.tel, N.tel, N.slv }, -- Вивек, Телванни
    ["3 -13"] = { N.cit, N.pil, N.pil }, -- Вивек, Храм
    ["4 -13"] = { N.pil }, -- Вивек, Храм

    ["1 -13"] = { N.cit }, -- Эбенгард
    ["2 -13"] = { N.cit }, -- Эбенгард

    ["6 -6"] = { N.cit, N.cit, N.slv }, -- Суран
    ["6 -7"] = { N.cit }, -- Суран

    ["-10 11"] = { N.cit, N.miner }, -- Гнисис
    ["-11 11"] = { N.pil, N.miner }, -- Гнисис

    ["-9 17"] = { N.cit, N.fish, N.fish }, -- Хуул

    ["-8 3"] = { N.cit, N.fish, N.fish }, -- Гнаар Мок

    ["-6 -5"] = { N.cit, N.fish, N.fish }, -- Хла Оуд

    ["7 22"] = { N.cit, N.fish }, -- Дагон Фел

    ["3 14"] = { N.cit, N.TMora, N.TMora, N.TMora, N.slv }, -- Тель Мора

    ["12 13"] = { N.cit, N.fish }, -- Вос
    ["11 14"] = { N.cit }, -- Вос

    ["10 14"] = { N.tel, N.tel, N.slv }, -- Тель Вос

    ["15 5"] = { N.tel, N.tel, N.slv, N.cit }, -- Тель Арун

    ["17 4"] = { N.tel, N.tel, N.slv, N.cit, N.fish }, -- Садрит Мора
    ["17 5"] = { N.tel }, -- Садрит Мора
    ["18 4"] = { N.tel, N.tel, N.slv, N.cit }, -- Садрит Мора

    ["14 -13"] = { N.tel, N.cit, N.fish }, -- Тель Бранора
    ["15 -13"] = { N.tel, N.tel, N.slv, N.cit }, -- Тель Бранора

    ["12 -8"] = { N.pil }, -- Молаг Мар
    ["13 -8"] = { N.pil, N.pil, N.citA }, -- Молаг Мар

    ["-3 12"] = { N.pil, N.red, N.citA }, -- Маар Ган

    ["-11 15"] = { N.cit, N.fish }, -- Альд Велоти

    ["2 4"] = { N.pil, N.red }, -- Призрачные Врата

    ["-2 2"] = { N.cit, N.miner, N.cit }, -- Кальдера
    ["-3 1"] = { N.miner }, -- Кальдера, шахты

    ["0 -8"] = { N.cit }, -- Пелагиад
    ["0 -7"] = { N.cit }, -- Пелагиад

}

local function npcsForCell(id)
    return Locations[id]
end

return {
    npcsForCell = npcsForCell,
}
